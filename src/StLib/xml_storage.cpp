#include "xml_storage.h"

using namespace std;

Actor domElementToActor(QDomElement &element)
{
    Actor a;
    a.id = element.attribute("id").toInt();
    a.name = element.attribute("name").toStdString();
    a.age = element.attribute("age").toInt();
    a.country = element.attribute("name").toStdString();
    a.filmAmount = element.attribute("filmAmount").toInt();
    return a;
}

Film domElementToFilm(QDomElement &element)
{
    Film f;
    f.id = element.attribute("id").toInt();
    f.name = element.attribute("name").toStdString();
    f.rating = element.attribute("rating").toInt();
    f.genre = element.attribute("genre").toStdString();
    f.yearOfIssue = element.attribute("yearOfIssue").toInt();
    return f;
}

QDomElement actorToDomElement(QDomDocument & doc,Actor &a)
{
    QDomElement actor_el = doc.createElement("actor");
    actor_el.setAttribute("id", a.id);
    actor_el.setAttribute("name", a.name.c_str());
    actor_el.setAttribute("age", a.age);
    actor_el.setAttribute("country", a.country.c_str());
    actor_el.setAttribute("filmAmount", a.filmAmount);
    return actor_el;
}

QDomElement filmToDomElement(QDomDocument & doc,Film &f)
{
    QDomElement film_el = doc.createElement("film");
    film_el.setAttribute("id", f.id);
    film_el.setAttribute("name", f.name.c_str());
    film_el.setAttribute("rating", f.rating);
    film_el.setAttribute("genre", f.genre.c_str());
    film_el.setAttribute("yearOfIssue", f.yearOfIssue);
    return film_el;
}

bool XmlStorage::load()
{
    //actors
    std::string filename = this->dir_name_ + "/actors.xml";
    QString q_filename = QString::fromStdString(filename);
    QFile file(q_filename);
    bool is_opened = file.open(QFile::ReadOnly);
    if(!is_opened)
    {
        qDebug()  << "File not opened" << q_filename;
        return false;
    }
    QTextStream ts(&file);
    QString text = ts.readAll();
    file.close();
    //
    QString errorMessage;
    int errorLine;
    int errorColumn;
    QDomDocument doc;
    if(!doc.setContent(text, &errorMessage, &errorLine, &errorColumn))
    {
        qDebug() << "Error parsing XML text: " << errorMessage;
        qDebug() << "at Line " << errorLine << "column" << errorColumn;
        return false;
    }
    QDomElement root = doc.documentElement();
    for(int i = 0; i < root.childNodes().size(); i++)
    {
        QDomNode node = root.childNodes().at(i);
        QDomElement element = node.toElement();
        Actor a = domElementToActor(element);
        this->actors_.push_back(a);
    }
    //
    //
    //
    //films
    std::string filename2 = this->dir_name_ + "/films.xml";
    QString q_filename2 = QString::fromStdString(filename2);
    QFile file2(q_filename2);
    bool is_opened2 = file2.open(QFile::ReadOnly);
    if(!is_opened2)
    {
        qDebug()  << "File not opened: " << q_filename2;
        return false;
    }
    QTextStream ts2(&file2);
    QString text2 = ts2.readAll();
    file2.close();
    //
    QString errorMessage2;
    int errorLine2;
    int errorColumn2;
    QDomDocument doc2;
    if(!doc2.setContent(text2, &errorMessage2, &errorLine2, &errorColumn2))
    {
        qDebug() << "Error parsing XML text: " << errorMessage2;
        qDebug() << "at Line " << errorLine2 << "column" << errorColumn2;
        return false;
    }
    QDomElement root2 = doc2.documentElement();
    for(int i = 0; i < root2.childNodes().size(); i++)
    {
        QDomNode node2 = root2.childNodes().at(i);
        QDomElement element2 = node2.toElement();
        Film f = domElementToFilm(element2);
        this->films_.push_back(f);
    }

    return true;

}

bool XmlStorage::save()
{
    //actors
    QDomDocument doc;

    QDomElement root = doc.createElement("actors");

    for(Actor &a: this->actors_)
    {
        QDomElement actor_el = actorToDomElement(doc, a);
        root.appendChild(actor_el);
    }

    doc.appendChild(root);
    QString xml_text = doc.toString(6);

    std::string filename = this->dir_name_ + "/actors.xml";
    QString q_filename = QString::fromStdString(filename);
    QFile file(q_filename);
    bool is_opened = file.open(QFile::WriteOnly);
    if(!is_opened)
    {
        qDebug()  << "File not opened for writing: " << q_filename;
        return false;
    }

    QTextStream ts(&file);
    ts << xml_text;
    file.close();
    //films
    QDomDocument doc2;

    QDomElement root2 = doc2.createElement("films");

    for(Film &f: this->films_)
    {
        QDomElement film_el = filmToDomElement(doc, f);
        root2.appendChild(film_el);
    }

    doc2.appendChild(root2);
    QString xml_text2 = doc2.toString(6);

    std::string filename2 = this->dir_name_ + "/films.xml";
    QString q_filename2 = QString::fromStdString(filename2);
    QFile file2(q_filename2);
    bool is_opened2 = file2.open(QFile::WriteOnly);
    if(!is_opened2)
    {
        qDebug()  << "File not opened for writing: " << q_filename2;
        return false;
    }

    QTextStream ts2(&file2);
    ts2 << xml_text2;
    file2.close();

    return true;
}
//ACTORS
vector<Actor> XmlStorage::getAllActors()
{
    return this->actors_;
}

bool XmlStorage::updateActor(const Actor &actor)
{
    for (int i = 0; this->actors_.size(); i++)
    {
        Actor item = this->actors_.at(i);

        if (actor.id == item.id)
        {
            this->actors_.at(i) = actor;
            return true;
        }
    }
    return false;
}

int XmlStorage::getNewActorId()
{
    int max_id = 0;
    for (Actor &c : this->actors_)
    {
        if (c.id > max_id)
        {
            max_id = c.id;
        }
    }
    int new_id = max_id + 1;
    return new_id;
}

int XmlStorage::insertActor(const Actor &actor)
{
    int new_id = this->getNewActorId();
    Actor copy = actor;
    copy.id = new_id;
    this->actors_.push_back(copy);
    return new_id;
}

bool XmlStorage::removeActor(int actor_id)
{
    int index = -1;
    for (size_t i = 0; i < this->actors_.size(); i++)
    {
        if (this->actors_[i].id == actor_id)
        {
            index = i;
            break;
        }
    }
    if (index >= 0)
    {
        this->actors_.erase(this->actors_.begin() + index);
        return true;
    }
    return false;
}

optional<Actor> XmlStorage::getActorById(int actor_id)
{
    for (Actor &c : this->actors_)
    {
        if (c.id == actor_id)
        {
            return c;
        }
    }

    return nullopt;
}
//
//
//FILMS
vector<Film> XmlStorage::getAllFilms()
{
    return this->films_;
}

bool XmlStorage::updateFilm(const Film &film)
{
    for (int i = 0; this->films_.size(); i++)
    {
        Film item = this->films_.at(i);

        if (film.id == item.id)
        {
            this->films_.at(i) = film;
            return true;
        }
    }
    return false;
}

int XmlStorage::getNewFilmId()
{
    int max_id = 0;
    for (Film &c : this->films_)
    {
        if (c.id > max_id)
        {
            max_id = c.id;
        }
    }
    int new_id = max_id + 1;
    return new_id;
}

int XmlStorage::insertFilm(const Film &film)
{
    int new_id = this->getNewFilmId();
    Film copy = film;
    copy.id = new_id;
    this->films_.push_back(copy);
    return new_id;
}

bool XmlStorage::removeFilm(int film_id)
{
    int index = -1;
    for (size_t i = 0; i < this->films_.size(); i++)
    {
        if (this->films_[i].id == film_id)
        {
            index = i;
            break;
        }
    }
    if (index >= 0)
    {
        this->films_.erase(this->films_.begin() + index);
        return true;
    }
    return false;
}

optional<Film> XmlStorage::getFilmById(int film_id)
{
    for (Film &c : this->films_)
    {
        if (c.id == film_id)
        {
            return c;
        }
    }

    return nullopt;
}
