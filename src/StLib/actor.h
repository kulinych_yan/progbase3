#pragma once

#include <string>
#include <QMetaType>

using std::string;

typedef struct Actor__
{
    int id;
    string name;
    int age;
    string country;
    int filmAmount;
    int user_id;
    string image;
} Actor;

Q_DECLARE_METATYPE(Actor)
