#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#include "storage.h"
#include <QSqlDatabase>
#include <iostream>
#include "md5.h"
#include "optional.h"
#include <QtSql>



class SqliteStorage : public Storage
{
    const string dir_name_;
    QSqlDatabase db_;

public:
    SqliteStorage(const string &dir_name);

   bool open();
   bool close();
  // actors
   vector<Actor> getAllActors(void);
   vector<Actor> getAllUsersActors(int user_id);
   optional<Actor> getActorById(int actor_id);
   bool updateActor(const Actor &actor);
   bool removeActor(int actor_id);
   int insertActor(const Actor &actor);
  // films
   vector<Film> getAllFilms(void);
   vector<Film> getAllUsersFilms(int user_id);
   optional<Film> getFilmById(int film_id);
   bool updateFilm(const Film &film);
   bool removeFilm(int film_id);
   int insertFilm(const Film &film);

   optional<User> get_auth(User info);
   int insertUser(const User &user);
   bool insertActorFilm(int actor_id, int film_id);
   vector<Actor> getAllFilmActors(int film_id);
   vector<Film>getAllActorFilms(int actor_id);
   bool removeActorFilm(int actor_id, int film_id);
   vector<User> getAllUsers(void);

};



#endif // SQLITE_STORAGE_H
