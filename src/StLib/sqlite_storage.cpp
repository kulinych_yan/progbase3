#include "sqlite_storage.h"


SqliteStorage::SqliteStorage(const string &dir_name) : dir_name_(dir_name)
{
    db_ = QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(this->dir_name_) + "/data.sqlite";
    cout << path.toStdString();
    db_.setDatabaseName(path);    // set sqlite database file path
    bool connected = db_.open();  // open db connection
    if (!connected)
    {
        return false;
    }

    return true;
}
bool SqliteStorage::close()
{
    db_.close();
    return true;
}

Actor getActorFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string name = query.value("name").toString().toStdString();
    int age = query.value("age").toInt();
    string country = query.value("country").toString().toStdString();
    int filmAmount = query.value("numberoffilms").toInt();
    string image = query.value("image").toString().toStdString();
    Actor a;
    a.id = id;
    a.name = name;
    a.age = age;
    a.country = country;
    a.filmAmount = filmAmount;
    a.image = image;
    return a;
}
Film getFilmFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string name = query.value("name").toString().toStdString();
    int rating = query.value("rating").toInt();
    string genre = query.value("genre").toString().toStdString();
    int yearOfIssue = query.value("yearofissue").toInt();
    Film f;
    f.id = id;
    f.name = name;
    f.rating = rating;
    f.genre = genre;
    f.yearOfIssue = yearOfIssue;
    return f;
}

User getUserFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string pass_hash = query.value("pass_hash").toString().toStdString();
    string username = query.value("username").toString().toStdString();
    User u;
    u.id = id;
    u.login = username;
    u.password = pass_hash;
    return u;
}

// actors
vector<Actor> SqliteStorage::getAllActors(void)
{
    vector<Actor> actors;
    QSqlQuery query("SELECT * FROM actors");
    if(!query.exec()){
        exit(67);
    }
    while (query.next())
    {
        Actor a = getActorFromQuery(query);
        actors.push_back(a);
    }
    return actors;
}

//vector<lecture> SqliteStorage::getAllUserLectures(int user_id)
//{
//    vector<lecture> lectures;
//    QSqlQuery query;
//    query.prepare("SELECT * FROM lectures WHERE user_id = :user_id");
//    query.bindValue(":user_id", user_id);
//    if (!query.exec())
//    {
//        qDebug() « query.lastError();
//    }
//    while (query.next())
//    {
//       lecture lect = getLectureFromQuery(query);
//       lectures.push_back(lect);
//    }
//    return lectures;
//}

vector<Actor> SqliteStorage::getAllUsersActors(int user_id)
{
    vector<Actor> actors;
    QSqlQuery query;
    query.prepare("SELECT * FROM actors where user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if(!query.exec()){
        qDebug() << query.lastError();
        exit(667);
    }
    while (query.next())
    {
        Actor a = getActorFromQuery(query);
        actors.push_back(a);
    }
    return actors;
}
optional<Actor> SqliteStorage::getActorById(int actor_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM actors WHERE id = :id");
    query.bindValue(":id", actor_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get actor error:" << query.lastError();
       return nullopt;
    }
    if (query.next()) {
        Actor a = getActorFromQuery(query);
;       return a;
    } else {
       return nullopt;
    }
}
bool SqliteStorage::updateActor(const Actor &actor)
{
    //qDebug() << actor.image;
    QSqlQuery query;
    query.prepare("UPDATE actors SET name = :name, age = :age, country = :country, numberoffilms = :numberoffilms, image = :image WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(actor.name));
    query.bindValue(":age", QString::number(actor.age));
    query.bindValue(":country", QString::fromStdString(actor.country));
    query.bindValue(":numberoffilms", QString::number(actor.filmAmount));
    query.bindValue(":id", QString::number(actor.id));
    query.bindValue(":image", QString::fromStdString(actor.image));
    if (!query.exec()){
        qDebug() << "update actor error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
bool SqliteStorage::removeActor(int actor_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM actors WHERE id = :id");
    query.bindValue(":id", actor_id);
    if (!query.exec()){
        qDebug() << "delete actor error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertActor(const Actor &actor)
{
    QSqlQuery query(this->db_);
    query.prepare("insert into actors (name , age , country, numberoffilms, user_id, image) values (:name , :age , :country , :numberoffilms, :user_id, :image)");
    query.bindValue(":name", QString::fromStdString(actor.name));
    query.bindValue(":age", actor.age);
    query.bindValue(":country", QString::fromStdString(actor.country));
    query.bindValue(":numberoffilms", actor.filmAmount);
    query.bindValue(":user_id", actor.user_id);
    query.bindValue(":image", actor.image.c_str());
    cout << actor.image;
    if (!query.exec()){
        cout << query.lastError().text().toStdString();
        exit(13);
    }

    QVariant var = query.lastInsertId();
    return var.toInt();
}
// films
vector<Film> SqliteStorage::getAllFilms(void)
{
    vector<Film> films;
    QSqlQuery query("SELECT * FROM films");
    if(!query.exec()){
        exit(668);
    }
    while (query.next())
    {
        Film f = getFilmFromQuery(query);
        films.push_back(f);
    }
    return films;
}
vector<Film> SqliteStorage::getAllUsersFilms(int user_id)
{
    vector<Film> films;
    QSqlQuery query;
    query.prepare("SELECT * FROM films where user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if(!query.exec()){
        exit(668);
    }
    while (query.next())
    {
        Film f = getFilmFromQuery(query);
        films.push_back(f);
    }
    return films;
}
optional<Film> SqliteStorage::getFilmById(int film_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM films WHERE id = :id");
    query.bindValue(":id", film_id);
    if (!query.exec()) {  // do exec if query is prepared SELECT query
       qDebug() << "get film error:" << query.lastError();
       return nullopt;
    }
    if (query.next()) {
        Film f = getFilmFromQuery(query);
;       return f;
    } else {
       return nullopt;
    }
}
bool SqliteStorage::updateFilm(const Film &film)
{
    QSqlQuery query;
    query.prepare("UPDATE films SET name = :name, rating = :rating, genre = :genre, yearOfIssue = :yearOfIssue WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(film.name));
    query.bindValue(":rating", QString::number(film.rating));
    query.bindValue(":genre", QString::fromStdString(film.genre));
    query.bindValue(":yearOfIssue", QString::number(film.yearOfIssue));
    query.bindValue(":id", QString::number(film.id));
    if (!query.exec()){
        qDebug() << "update film error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
bool SqliteStorage::removeFilm(int film_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM films WHERE id = :id");
    query.bindValue(":id", film_id);
    if (!query.exec()){
        qDebug() << "delete film error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertFilm(const Film &film)
{
    QSqlQuery query(this->db_);
    query.prepare("insert into films (name , rating , genre, yearOfIssue, user_id) values (:name , :rating , :genre , :yearOfIssue, :user_id)");
    query.bindValue(":name", QString::fromStdString(film.name));
    query.bindValue(":rating", film.rating);
    query.bindValue(":genre", QString::fromStdString(film.genre));
    query.bindValue(":yearOfIssue", film.yearOfIssue);
    query.bindValue(":user_id", film.user_id);
    if (!query.exec()){
        cout << query.lastError().text().toStdString();
        exit(1111);
    }

    QVariant var = query.lastInsertId();
    return var.toInt();
}

//
//
//auth

int SqliteStorage::insertUser(const User &user)
{
    qDebug() <<"LOGIN: "<<QString::fromStdString(user.login);
    qDebug() <<"PASS_HASH: "<<QString::fromStdString(user.password);
    QSqlQuery query(this->db_);
    query.prepare("INSERT INTO users (username, pass_hash) VALUES (:username, :pass_hash)");
    query.bindValue(":username", QString::fromStdString(user.login));
    query.bindValue(":pass_hash", QString::fromStdString(user.password));

    if (!query.exec()){
        qDebug() << query.lastError();
    }
    //qDebug() << query.lastError();
    QVariant var = query.lastInsertId();
    return var.toInt();
}

optional<User> SqliteStorage::get_auth(User info)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username = :username and pass_hash = :pass_hash");
    query.bindValue(":username", QString::fromStdString(info.login));
    query.bindValue(":pass_hash", QString::fromStdString(info.password));
    if(!query.exec())
    {
        qDebug() << query.lastError();
    }

    int i = 0;
    while(query.next())
    {
        info.id = query.value("id").toInt();
        i++;
    }
    if(i == 1)
    {
        return info;
    }
    else
    {
        qDebug() << "Incorrect login or password";
        return nullopt;
    }
}

//bool SqliteStorage::get_auth(User info){
//    QSqlQuery query;
//    query.prepare("select * from users where username = :username and pass_hash = :pass_hash");
//    query.bindValue(":username", QString::fromStdString(info.password));
//    query.bindValue(":pass_hash", QString::fromStdString(md5(info.password)));

//    if(!query.exec())
//    {
//        qDebug() << query.lastError();
//        abort();
//    }
//    else
//    {
//       int i = 0;
//       while(query.next())
//       {
//           int user_id = query.value("id").toInt();
//           i++;
//       }

//        if(i == 1)
//        {
//            return true;
//        }
//        else
//        {
//            return false;
//        }
//    }
//}

//links
//
//
bool SqliteStorage::insertActorFilm(int actor_id, int film_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO links (actor_id,film_id) VALUES (:actor_id,:film_id)");
    query.bindValue(":actor_id",actor_id);
    query.bindValue(":film_id", film_id);
    if (!query.exec())
    {
        qDebug() << "addLink error:" << query.lastError();
        return false;
    }
    return true;
}

vector<Actor> SqliteStorage::getAllFilmActors(int film_id)
{
    vector<Actor> actors;
    QSqlQuery query;
    query.prepare ("SELECT * FROM actors JOIN links WHERE actors.id = links.actor_id AND links.film_id = :film_id");
    query.bindValue ( ":film_id", film_id);
    if (!query.exec())
    {
        return actors;
    }
    while (query.next())
    {
        Actor actor;
        actor.id = query.value("id").toInt();
        actor.name = query.value ( "name").toString().toStdString();
        actor.country = query.value ( "country").toString().toStdString();
        actor.age = query.value("age").toInt();
        actor.filmAmount = query.value("numberoffilms").toInt();
        actors.push_back(actor);
    }
    return actors;
}

vector<Film> SqliteStorage::getAllActorFilms(int actor_id)
{
    vector<Film> films;
    QSqlQuery query;
    query.prepare ("SELECT * FROM films JOIN links WHERE films.id = links.film_id AND links.actor_id = :actor_id");
    query.bindValue ( ":actor_id", actor_id);
    if (!query.exec())
    {
        return films;
    }
    while (query.next())
    {
        Film f = getFilmFromQuery(query);
        films.push_back(f);
    }
    qDebug() << "OK!";
    return films;
}

bool SqliteStorage::removeActorFilm(int actor_id, int film_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE actor_id = :actor_id AND film_id = :film_id" );
    query.bindValue(":actor_id",actor_id);
    query.bindValue(":film_id", film_id);
    if (!query.exec())
    {
        qDebug() << "deleteLink error:"
                 << query.lastError();
        return false;
    }
    return true;
}


vector<User> SqliteStorage::getAllUsers(void)
{
    vector<User> users;
    QSqlQuery query("SELECT * FROM users");
    if(!query.exec()){
        exit(111);
    }
    while (query.next())
    {
        User u = getUserFromQuery(query);
        users.push_back(u);
    }
    return users;
}
