#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.h"
#include "actor.h"
#include "film.h"
#include "user.h"
//#include "sqlite_storage.h"
//#include "xml_storage.h"

using namespace std;

class Storage
{
 public:
   virtual ~Storage() {}  // UPD (8.5.2019): added 

   virtual bool open() = 0;
   virtual bool close() = 0;
   // actors
   virtual vector<Actor> getAllActors(void) = 0;
   virtual vector<Actor> getAllUsersActors(int user_id) = 0;
   virtual optional<Actor> getActorById(int actor_id) = 0;
   virtual bool updateActor(const Actor &actor) = 0;
   virtual bool removeActor(int actor_id) = 0;
   virtual int insertActor(const Actor &actor) = 0;
   // films
   virtual vector<Film> getAllFilms(void) = 0;
   virtual vector<Film> getAllUsersFilms(int user_id) = 0;
   virtual optional<Film> getFilmById(int film_id) = 0;
   virtual bool updateFilm(const Film &film) = 0;
   virtual bool removeFilm(int film_id) = 0;
   virtual int insertFilm(const Film &film) = 0;

   virtual optional<User> get_auth(User info) = 0;
   virtual int insertUser(const User &user) = 0;
   virtual bool insertActorFilm(int actor_id, int film_id) = 0;
   virtual vector<Actor> getAllFilmActors(int film_id) = 0;
   virtual vector<Film>getAllActorFilms(int actor_id) = 0;
   virtual bool removeActorFilm(int actor_id, int film_id) = 0;

   virtual vector<User> getAllUsers(void) = 0;
};
