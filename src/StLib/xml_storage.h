#pragma once

#include <vector>
#include <string>

#include "optional.h"
#include "actor.h"
#include "film.h"
#include "storage.h"
#include <QDomDocument>
#include <fstream>
#include <iostream>
#include <string>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QtXml>

using std::string;
using std::vector;

class XmlStorage :  public Storage
{
  const string dir_name_;

  vector<Actor> actors_; // UPD for future (8.5.2019): add methods:
  vector<Film> films_;

  int getNewActorId();
  int getNewFilmId();

public:
  XmlStorage(const string &dir_name) : dir_name_(dir_name) {}

  bool load();
  bool save();
  // actors
  vector<Actor> getAllActors(void);
  optional<Actor> getActorById(int actor_id);
  bool updateActor(const Actor &actor);
  bool removeActor(int actor_id);
  int insertActor(const Actor &actor);
  //films
  vector<Film> getAllFilms(void);
  optional<Film> getFilmById(int film_id);
  bool updateFilm(const Film &film);
  bool removeFilm(int film_id);
  int insertFilm(const Film &film);
};
