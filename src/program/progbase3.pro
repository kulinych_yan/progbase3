#-------------------------------------------------
#
# Project created by QtCreator 2019-06-01T07:55:51
#
#-------------------------------------------------

QT       += core gui xml sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = progbase3
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    add_button_actor.cpp \
    add_button_film.cpp \
    edit_button_actor.cpp \
    edit_button_film.cpp \
    #xml_storage.cpp \
    auth.cpp \
    #sqlite_storage.cpp \
    #md5.cpp \
    registration.cpp \
    remove_button_actor.cpp \
    remove_button_film.cpp \
    add_link_button.cpp \

HEADERS += \
        mainwindow.h \
    add_button_actor.h \
    add_button_film.h \
    edit_button_actor.h \
    edit_button_film.h \
    #xml_storage.h \
    #storage.h \
    #optional.h \
    #film.h \
    #actor.h \
    auth.h \
    #user.h \
    #sqlite_storage.h \
    #md5.h \
    registration.h \
    remove_button_actor.h \
    remove_button_film.h \
    add_link_button.h \
    mainwindow.cpp.autosave \
    #xml_storage.h

FORMS += \
        mainwindow.ui \
    add_button_actor.ui \
    add_button_film.ui \
    edit_button_actor.ui \
    edit_button_film.ui \
    auth.ui \
    registration.ui \
    remove_button_actor.ui \
    remove_button_film.ui \
    add_link_button.ui

DISTFILES += \
    .gitignore \
    data/xml/actors.xml \
    data/xml/films.xml \

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-StLib-Desktop-Debug/release/ -lStLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-StLib-Desktop-Debug/debug/ -lStLib
else:unix: LIBS += -L$$PWD/../build-StLib-Desktop-Debug/ -lStLib

INCLUDEPATH += $$PWD/../StLib
DEPENDPATH += $$PWD/../StLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-StLib-Desktop-Debug/release/libStLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-StLib-Desktop-Debug/debug/libStLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-StLib-Desktop-Debug/release/StLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-StLib-Desktop-Debug/debug/StLib.lib
else:unix: PRE_TARGETDEPS += $$PWD/../build-StLib-Desktop-Debug/libStLib.a
