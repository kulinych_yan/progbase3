#include "mainwindow.h"
#include "ui_mainwindow.h"

string & actorGetName(Actor * self);
string & filmGetName(Film * self);
int & filmGetId(Film * self);
int & actorGetId(Actor * self);


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QString file_name = "/home/yan/progbase3/src/program/data/sql";
    qDebug() << file_name;
//    const string file_name  = "/home/yan/progbase3/data";
    if(storage_ == NULL)
    {
        qDebug() << "storage = NULL";
        delete storage_;
    }
    if(file_name == NULL)
    {
        qDebug() << "file = NULL";
    }

    SqliteStorage * sql_storage = new SqliteStorage(file_name.toStdString());
    Storage * storage = sql_storage;
    this->storage_ = storage;
    if(!this->storage_->open())
    {
        qDebug() << "storage not opened";
    }
    else{
        auth dAuth;
        connect(this, SIGNAL(send_reply(optional<User>)), &dAuth, SLOT(get_auth(optional<User>)));
        connect(&dAuth, SIGNAL(send_data(User)), this, SLOT(recieve_user_input(User)));
        dAuth.setModal(true);
        dAuth.exec();
    }

    ui->setupUi(this);
    ui->actor_button->setEnabled(true);
    ui->film_button->setEnabled(true);
    ui->add_button->setEnabled(false);
    ui->edit_button->setEnabled(false);
    ui->remove_button->setEnabled(false);

    keyCtrlO = new QShortcut(this);
    keyCtrlO->setKey(Qt::CTRL + Qt::Key_O);

    keyCtrlN = new QShortcut(this);
    keyCtrlN->setKey(Qt::CTRL + Qt::Key_N);
//    int w = ui->label_5->width();
//    int h = ui->label_5->height();
//    QPixmap pix("/home/yan/progbase3/data/img/PENTAGON-Hui-XPN.jpg");
//    ui->label_5->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));



    connect(keyCtrlO, SIGNAL(activated()), this, SLOT( on_actionOpen_storage_triggered()));
    connect(keyCtrlN, SIGNAL(activated()), this, SLOT(on_actionNew_storage_triggered()));
}
void MainWindow::recieve_user_input(User info){
    optional<User> replay = this->storage_->get_auth(info);
    send_reply(replay);
    if(replay)
    {
       this->user_id = replay.value().id;

    }
}

MainWindow::~MainWindow()
{
    delete keyCtrlN;
    delete keyCtrlO;
    delete storage_;
    delete ui;
}

void MainWindow::fill_list_info_actors()
{
    ui->info_list->clear();
    vector <Actor> vec = storage_->getAllUsersActors(this->user_id);
    //vector <Actor> vec = storage_->getAllActors();
    for (size_t i = 0; i < vec.size(); i++)
    {
        QVariant var;
        Actor cur = vec.at(i);
        var.setValue(cur);
        QListWidgetItem *item = new QListWidgetItem();
        item->setText(QString::fromStdString(actorGetName(&cur)));
        item->setData(Qt::UserRole, var);
        ui->info_list->addItem(item);
    }
    return;
//    auto actors = this->storage_->getAllActors();
//    for(size_t i = 0; i < actors.size(); i++ )
//    {
//        Actor a = actors.at(i);
//        QString info = QString::fromStdString(a.name);
//        ui->info_list->addItem(info);
//    }
}
void MainWindow::fill_list_info_films()
{
    ui->info_list->clear();
    qDebug() << user_id;
    vector <Film> vec = storage_->getAllUsersFilms(user_id);
    //vector <Film> vec = storage_->getAllFilms();
    for (size_t i = 0; i < vec.size(); i++)
    {
        QVariant var;
        Film cur = vec.at(i);
        var.setValue(cur);
        QListWidgetItem *item = new QListWidgetItem();
        item->setText(QString::fromStdString(filmGetName(&cur)));
        item->setData(Qt::UserRole, var);
        ui->info_list->addItem(item);
    }
//    auto films = this->storage_->getAllFilms();
//    for(size_t i = 0; i < films.size(); i++ )
//    {
//        Film f = films.at(i);
//        QString info = QString::fromStdString(f.name);
//        ui->info_list->addItem(info);
//    }
}
void MainWindow::fill_info_list()
{
    if(this->selected_entity == "actor")
    {
        fill_list_info_actors();
    }
    if(this->selected_entity == "film")
    {
        fill_list_info_films();
    }
}
//
string & MainWindow::actorGetName(Actor * self)
{
    return self->name;
}
string & MainWindow::filmGetName(Film * self)
{
    return self->name;
}


void MainWindow::update_info_list()
{
    ui->label_id_2->clear();
    ui->label_name_2->clear();
    ui->label_2->clear();
    ui->label_last_2->clear();
    ui->label_4->clear();
    this->ui->info_list->clear();
    this->fill_info_list();
}

void MainWindow::on_actor_button_clicked()
{
    this->selected_entity = "actor";
    ui->label_id->clear();
    ui->label_name->clear();
    ui->label_1->clear();
    ui->label_last->clear();
    ui->label_3->clear();
    this->update_info_list();
    ui->add_button->setEnabled(true);
    ui->edit_button->setEnabled(false);
    ui->remove_button->setEnabled(false);
}
void MainWindow::on_film_button_clicked()
{
    this->selected_entity = "film";
    ui->label_id->clear();
    ui->label_name->clear();
    ui->label_1->clear();
    ui->label_last->clear();
    ui->label_3->clear();
    ui->label_5->clear();
    this->update_info_list();
    ui->add_button->setEnabled(true);
    ui->edit_button->setEnabled(false);
    ui->remove_button->setEnabled(false);
}

void MainWindow::on_add_button_clicked()
{
    if(this->selected_entity == "actor")
    {
        add_button_actor add_button;
        connect(&add_button, SIGNAL(send_actor(Actor)), this, SLOT(recieve_new_actor(Actor)));
        add_button.setModal(true);
        add_button.exec();
    }
    if(this->selected_entity == "film")
    {
        add_button_film add_button;
        connect(&add_button, SIGNAL(send_film(Film)), this, SLOT(recieve_new_film(Film)));
        add_button.setModal(true);
        add_button.exec();
    }
    this->ui->label_id_2->clear();
    this->ui->label_name_2->clear();
    this->ui->label_2->clear();
    this->ui->label_last_2->clear();
    this->ui->label_4->clear();
    this->ui->label_id->clear();
    this->ui->label_name->clear();
    this->ui->label_1->clear();
    this->ui->label_last->clear();
    this->ui->label_3->clear();
    this->ui->label_5->clear();
    this->ui->remove_button->setEnabled(false);
    this->ui->edit_button->setEnabled(false);
}
void MainWindow::recieve_new_actor(Actor a)
{
    a.user_id = user_id;
    this->ui->info_list->clear();
    this->storage_->insertActor(a);
    this->fill_list_info_actors();
}
void MainWindow::recieve_new_film(Film f)
{
    f.user_id = user_id;
    this->ui->info_list->clear();
    this->storage_->insertFilm(f);
    this->fill_list_info_films();
}

void MainWindow::on_edit_button_clicked()
{
    if(this->selected_entity == "actor")
    {
        edit_button_actor edit_button;
        QListWidgetItem * item = this->ui->info_list->currentItem();
        QVariant var = item->data(Qt::UserRole);
        Actor a = var.value<Actor>();
        int id = actorGetId(&a);
        qDebug() << id;
        Actor actor_a = this->storage_->getActorById(id).value();
        connect(this, SIGNAL(send_actor(Actor)), &edit_button, SLOT(receive_actor(Actor)));
        connect(&edit_button, SIGNAL(send_edited_actor(Actor)), this, SLOT(recieve_edited_actor(Actor)));
        connect(this, SIGNAL(send_user_id(int)), &edit_button, SLOT(recieve_user_id(int)));
        this->send_actor(actor_a);
        this->send_user_id(user_id);
        edit_button.setModal(true);
        edit_button.exec();
        this->update_info_list();
    }
    if(this->selected_entity == "film")
    {
        edit_button_film edit_button;
        QListWidgetItem * item = this->ui->info_list->currentItem();
        QVariant var = item->data(Qt::UserRole);
        Film a = var.value<Film>();
        int id = filmGetId(&a);
        qDebug() << id;
        Film film_f = this->storage_->getFilmById(id).value();
        connect(this, SIGNAL(send_film(Film)), &edit_button, SLOT(receive_film(Film)));
        connect(&edit_button, SIGNAL(send_edited_film(Film)), this, SLOT(recieve_edited_film(Film)));
        this->send_film(film_f);
        edit_button.setModal(true);
        edit_button.exec();
        this->update_info_list();
    }
    this->ui->label_id->clear();
    this->ui->label_name->clear();
    this->ui->label_1->clear();
    this->ui->label_last->clear();
    this->ui->label_3->clear();
    //this->ui->label->clear();
    this->ui->label_5->clear();
    this->ui->remove_button->setEnabled(false);
    this->ui->edit_button->setEnabled(false);
}
void MainWindow::recieve_edited_actor(Actor a)
{
    this->storage_->updateActor(a);
    this->update_info_list();
}
void MainWindow::recieve_edited_film(Film f)
{
    this->storage_->updateFilm(f);
    this->update_info_list();
}

void MainWindow::on_remove_button_clicked()
{
    if(this->selected_entity == "actor")
    {
        remove_button_actor remove;
        connect(&remove, SIGNAL(confirm_remove()),this, SLOT(remove_actor()));
        remove.setModal(true);
        remove.exec();
        this->update_info_list();
        this->ui->label_id_2->clear();
        this->ui->label_name_2->clear();
        this->ui->label_2->clear();
        this->ui->label_last_2->clear();
        this->ui->label_4->clear();
    }
    if(this->selected_entity == "film")
    {
        remove_button_film remove;
        connect(&remove, SIGNAL(confirm_remove()),this, SLOT(remove_film()));
        remove.setModal(true);
        remove.exec();
        this->update_info_list();
        this->ui->label_id_2->clear();
        this->ui->label_name_2->clear();
        this->ui->label_2->clear();
        this->ui->label_last_2->clear();
        this->ui->label_4->clear();
    }
    this->ui->label_id->clear();
    this->ui->label_name->clear();
    this->ui->label_1->clear();
    this->ui->label_last->clear();
    this->ui->label_3->clear();
    //this->ui->label->clear();
    this->ui->label_5->clear();
    this->ui->remove_button->setEnabled(false);
    this->ui->edit_button->setEnabled(false);
}
void MainWindow::remove_actor()
{
    QListWidgetItem * item = this->ui->info_list->currentItem();
    QVariant var = item->data(Qt::UserRole);
    Actor a = var.value<Actor>();
    int id = actorGetId(&a);
    qDebug() << id;
    this->storage_->removeActor(id);
}
void MainWindow::remove_film()
{
    QListWidgetItem * item = this->ui->info_list->currentItem();
    QVariant var = item->data(Qt::UserRole);
    Film f = var.value<Film>();
    int id = filmGetId(&f);
    qDebug() << id;
    this->storage_->removeFilm(id);
}

int & MainWindow::filmGetId(Film * self)
{
    return self->id;
}
int & MainWindow::actorGetId(Actor * self)
{
    return self->id;
}
void MainWindow::on_info_list_itemClicked(QListWidgetItem *item)
{
    ui->edit_button->setEnabled(true);
    ui->remove_button->setEnabled(true);
    QVariant var = item->data(Qt::UserRole);
    int id = 0;
    if(this->selected_entity == "actor")
    {
        Actor a = var.value<Actor>();
        id = actorGetId(&a);
        Actor actor = this->storage_->getActorById(id).value();
        ui->label_id->setText("ID :");
        ui->label_name->setText("NAME :");
        ui->label_1->setText("AGE :");
        ui->label_last->setText("COUNTRY :");
        ui->label_3->setText("FILM AMOUNT :");
        this->ui->label_id_2->setText(QString::number(actor.id));
        this->ui->label_name_2->setText(QString::fromStdString(actor.name));
        this->ui->label_2->setText(QString::number(actor.age));
        this->ui->label_last_2->setText(QString::fromStdString(actor.country));
        this->ui->label_4->setText(QString::number(actor.filmAmount));
        int w = ui->label_5->width();
        int h = ui->label_5->height();
        string image_name = actor.image;
        QPixmap pix(image_name.c_str());
        ui->label_5->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
    }
    else if(this->selected_entity == "film")
    {
        Film f = var.value<Film>();
        id = filmGetId(&f);
        Film film = this->storage_->getFilmById(id).value();
        ui->label_id->setText("ID :");
        ui->label_name->setText("NAME :");
        ui->label_1->setText("RATING :");
        ui->label_last->setText("GENRE :");
        ui->label_3->setText("RELEASE DATE :");
        this->ui->label_id_2->setText(QString::number(film.id));
        this->ui->label_name_2->setText(QString::fromStdString(film.name));
        this->ui->label_2->setText(QString::number(film.rating));
        this->ui->label_last_2->setText(QString::fromStdString(film.genre));
        this->ui->label_4->setText(QString::number(film.yearOfIssue));
    }
}

void MainWindow::on_actionClose_triggered()
{
    this->close();
    storage_->close();
}

void MainWindow::on_actionNew_storage_triggered()
{
//    QString file_name = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"/home/yan/progbase2/labs/lab7/data", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

//    delete storage_;
//    XmlStorage  * xml_storage = new XmlStorage(file_name.toStdString());
//    Storage * storage_ptr = xml_storage;
//    this->storage_ = storage_ptr;
//    this->storage_->load();
//    ui->actor_button->setEnabled(true);
//    ui->film_button->setEnabled(true);
}

void MainWindow::on_actionOpen_storage_triggered()
{
    QString file_name = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"/home/yan/progbase3/data",QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks);
    qDebug() << file_name;
    if(storage_ == NULL)
    {
        qDebug() << "storage = NULL";
        delete storage_;
    }
    if(file_name == NULL)
    {
        qDebug() << "file = NULL";
    }

    SqliteStorage * sql_storage = new SqliteStorage(file_name.toStdString());
    Storage * storage = sql_storage;
    this->storage_ = storage;
    if(!this->storage_->open())
    {
        qDebug() << "storage not opened";
    }
    else{
        auth dAuth;
        connect(this, SIGNAL(send_reply(optional<User>)), &dAuth, SLOT(get_auth(optional<User>)));
        connect(&dAuth, SIGNAL(send_data(User)), this, SLOT(recieve_user_input(User)));
        dAuth.setModal(true);
        dAuth.exec();
    }

}

void MainWindow::on_lineEdit_textEdited(const QString &arg1)
{
    ui->info_list->clear();
    QString str1 = arg1;
    if (str1 != "")
    {
        if (this->selected_entity == "actor")
        {
            vector<Actor> vec = storage_->getAllUsersActors(this->user_id);
            //vector<Actor> vec = storage_->getAllActors();
            for (size_t i = 0; i < vec.size(); i++)
            {
                QVariant var;
                Actor cur = vec.at(i);
                const char *hyp = actorGetName(&cur).c_str();
                QString str2(QString::fromLatin1(hyp));
                var.setValue(cur);
                if ((str2.contains(str1)))
                {
                    QListWidgetItem *item = new QListWidgetItem();
                    item->setText(QString::fromStdString(actorGetName(&cur)));
                    item->setData(Qt::UserRole, var);
                    ui->info_list->addItem(item);
                }
            }
        }
        if (this->selected_entity == "film")
        {
            vector<Film> vec = storage_->getAllUsersFilms(this->user_id);
            //vector<Film> vec = storage_->getAllFilms();
            for (size_t i = 0; i < vec.size(); i++)
            {
                QVariant var;
                Film cur = vec.at(i);
                const char *hyp = filmGetName(&cur).c_str();
                QString str2(QString::fromLatin1(hyp));
                var.setValue(cur);
                if (str2.contains(str1))
                {
                    QListWidgetItem *item = new QListWidgetItem();
                    item->setText(QString::fromStdString(filmGetName(&cur)));
                    item->setData(Qt::UserRole, var);
                    ui->info_list->addItem(item);
                }
            }
        }
    }
    else
    {
        fill_info_list();
    }
}

Actor domElementToActor(QDomElement &element)
{
    Actor a;
    a.id = element.attribute("id").toInt();
    a.name = element.attribute("name").toStdString();
    a.age = element.attribute("age").toInt();
    a.country = element.attribute("name").toStdString();
    a.filmAmount = element.attribute("filmAmount").toInt();
    return a;
}
Film domElementToFilm(QDomElement &element)
{
    Film f;
    f.id = element.attribute("id").toInt();
    f.name = element.attribute("name").toStdString();
    f.rating = element.attribute("rating").toInt();
    f.genre = element.attribute("genre").toStdString();
    f.yearOfIssue = element.attribute("yearOfIssue").toInt();
    return f;
}

bool MainWindow::xml_load_actors(string file_name)
{
    vector<Actor> actors;

    QFile file(QString::fromStdString(file_name));
    bool is_opened = file.open(QFile::ReadOnly);
    if(!is_opened)
    {
        qDebug()  << "File not opened: " << QString::fromStdString(file_name);
        return false;
    }
    QTextStream ts(&file);
    QString text = ts.readAll();
    file.close();
    //
    QString errorMessage;
    int errorLine;
    int errorColumn;
    QDomDocument doc;
    if(!doc.setContent(text, &errorMessage, &errorLine, &errorColumn))
    {
        qDebug() << "Error parsing XML text: " << errorMessage;
        qDebug() << "at Line " << errorLine << "column" << errorColumn;
        return false;
    }
    QDomElement root = doc.documentElement();
    for(int i = 0; i < root.childNodes().size(); i++)
    {
        QDomNode node = root.childNodes().at(i);
        if(node.isElement())
        {
            QDomElement element = node.toElement();
            Actor a = domElementToActor(element);
            actors.push_back(a);
        }
    }

    for(Actor & actor : actors)
    {
        Actor a = actor;
        a.user_id = user_id;
    }

    return true;
}
bool MainWindow::xml_load_films(string file_name)
{
    vector<Film> films;

    QFile file(QString::fromStdString(file_name));
    bool is_opened = file.open(QFile::ReadOnly);
    if(!is_opened)
    {
        qDebug()  << "File not opened: " << QString::fromStdString(file_name);
        return false;
    }
    QTextStream ts(&file);
    QString text = ts.readAll();
    file.close();
    //
    QString errorMessage;
    int errorLine;
    int errorColumn;
    QDomDocument doc;
    if(!doc.setContent(text, &errorMessage, &errorLine, &errorColumn))
    {
        qDebug() << "Error parsing XML text: " << errorMessage;
        qDebug() << "at Line " << errorLine << "column" << errorColumn;
        return false;
    }
    QDomElement root = doc.documentElement();
    for(int i = 0; i < root.childNodes().size(); i++)
    {
        QDomNode node = root.childNodes().at(i);
        QDomElement element = node.toElement();
        Film f = domElementToFilm(element);
        films.push_back(f);
    }
    for(Film & film : films)
    {
        Film f = film;
        f.user_id = user_id;
    }
    return true;
}

void MainWindow::on_actionImport_actors_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
                   this,              // parent
                   "Dialog Caption",  // caption
                   "/home/yan/progbase3/src/program/data",                // directory to start with
                   "All Files (*)");  // file name filter
    if(!fileName.isEmpty())
    {
        this->xml_load_actors(fileName.toStdString());
    }
    this->ui->info_list->clear();
    ui->actor_button->setEnabled(true);
    ui->film_button->setEnabled(true);
    ui->add_button->setEnabled(false);
    ui->edit_button->setEnabled(false);
    ui->remove_button->setEnabled(false);
}
void MainWindow::on_actionImport_films_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
                   this,              // parent
                   "Dialog Caption",  // caption
                   "/home/yan/progbase3/src/program/data",                // directory to start with
                   "All Files (*)");  // file name filter
    if(!fileName.isEmpty())
    {
        this->xml_load_films(fileName.toStdString());
    }
    this->ui->info_list->clear();
    ui->actor_button->setEnabled(true);
    ui->film_button->setEnabled(true);
    ui->add_button->setEnabled(false);
    ui->edit_button->setEnabled(false);
    ui->remove_button->setEnabled(false);
}

QDomElement actorToDomElement(QDomDocument & doc,Actor &a)
{
    QDomElement actor_el = doc.createElement("actor");
    actor_el.setAttribute("id", a.id);
    actor_el.setAttribute("name", a.name.c_str());
    actor_el.setAttribute("age", a.age);
    actor_el.setAttribute("country", a.country.c_str());
    actor_el.setAttribute("filmAmount", a.filmAmount);
    return actor_el;
}
QDomElement filmToDomElement(QDomDocument & doc,Film &f)
{
    QDomElement film_el = doc.createElement("film");
    film_el.setAttribute("id", f.id);
    film_el.setAttribute("name", f.name.c_str());
    film_el.setAttribute("rating", f.rating);
    film_el.setAttribute("genre", f.genre.c_str());
    film_el.setAttribute("yearOfIssue", f.yearOfIssue);
    return film_el;
}

void MainWindow::on_actionExport_actors_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(
                    this,              // parent
                    "Dialog Caption",  // caption
                    "/home/yan/progbase3/src/program/data/xml",                // directory to start with
                    "",
                    nullptr,
                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);  // file name filter

        vector<Actor> actors = this->storage_->getAllUsersActors(user_id);

        QDomDocument doc;
        QDomElement root = doc.createElement("actors");

        for(Actor & actor : actors)
        {
            QDomElement actor_el =  actorToDomElement(doc, actor);
            root.appendChild(actor_el);
        }

        doc.appendChild(root);
        QString xml_text = doc.toString(6);
        std::string filename = fileName.toStdString();
        QString q_filename = QString::fromStdString(filename);
        QFile file(q_filename);
        bool is_opened = file.open(QFile::WriteOnly);
        if(!is_opened)
        {
            qDebug()  << "File not opened for writing: " << q_filename;
        }

        QTextStream ts(&file);
        ts << xml_text;
        file.close();
        return;
}
void MainWindow::on_actionExport_films_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(
                    this,              // parent
                    "Dialog Caption",  // caption
                    "/home/yan/progbase3/src/program/data/xml",                // directory to start with
                    "",
                    nullptr,
                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);  // file name filter

        vector<Film> films = this->storage_->getAllUsersFilms(user_id);

        QDomDocument doc;
        QDomElement root = doc.createElement("films");

        for(Film & film : films)
        {
            QDomElement film_el =  filmToDomElement(doc, film);
            root.appendChild(film_el);
        }

        doc.appendChild(root);
        doc.appendChild(root);
        QString xml_text = doc.toString(6);
        std::string filename = fileName.toStdString();
        QString q_filename = QString::fromStdString(filename);
        QFile file(q_filename);
        bool is_opened = file.open(QFile::WriteOnly);
        if(!is_opened)
        {
            qDebug()  << "File not opened for writing: " << q_filename;
        }

        QTextStream ts(&file);
        ts << xml_text;
        file.close();
        return;
}
