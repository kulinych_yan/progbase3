#include "add_button_actor.h"
#include "ui_add_button_actor.h"

add_button_actor::add_button_actor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add_button_actor)
{
    ui->setupUi(this);
}

add_button_actor::~add_button_actor()
{
    delete ui;
}

void add_button_actor::on_ok_button_clicked()
{
    Actor a;
    a.country = ui->line_country->text().toStdString();
    a.name = ui->line_name->text().toStdString();
    a.age = ui->spinBox_age->value();
    a.filmAmount = ui->spinBox_film_amount->value();
    this->send_actor(a);
    this->close();
}

void add_button_actor::on_cancel_button_clicked()
{
    this->close();
}
