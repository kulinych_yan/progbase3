#include "add_link_button.h"
#include "ui_add_link_button.h"
#include <QDebug>

string & add_link_button::filmGetName(Film * self)
{
    return self->name;
}

int & add_link_button::filmGetId(Film * self)
{
    return self->id;
}

add_link_button::add_link_button(QWidget *parent, int user) :
    QDialog(parent),
    ui(new Ui::add_link_button)
{
    QString file_name = "/home/yan/progbase3/src/program/data/sql";
    qDebug() << file_name;
    user_id = user;
    if(storage_ == NULL)
    {
        qDebug() << "storage = NULL";
        delete storage_;
    }
    if(file_name == NULL)
    {
        qDebug() << "file = NULL";
    }

    SqliteStorage * sql_storage = new SqliteStorage(file_name.toStdString());
    Storage * storage = sql_storage;
    this->storage_ = storage;
    if(!this->storage_->open())
    {
        qDebug() << "storage not opened";
    }
    ui->setupUi(this);
    this->fill_films_list();
    this->fill_films_list();
    ui->add_button->setEnabled(false);
}

add_link_button::~add_link_button()
{
    delete ui;
}

void add_link_button::on_add_button_clicked()
{
    qDebug() << this->actor_id << "<- actor id || film id ->" << this->film_id;
    this->storage_->insertActorFilm(this->actor_id, this->film_id);
    this->fill_films_list();
    ui->add_button->setEnabled(false);
}

void add_link_button::on_close_button_clicked()
{
    this->close();
}

void add_link_button::fill_films_list()
{
    ui->films_list->clear();
    vector <Film> vec = storage_->getAllUsersFilms(user_id);
    for (size_t i = 0; i < vec.size(); i++)
    {
        QVariant var;
        Film cur = vec.at(i);
        var.setValue(cur);
        QListWidgetItem *item = new QListWidgetItem();
        item->setText(QString::fromStdString(filmGetName(&cur)));
        item->setData(Qt::UserRole, var);
        ui->films_list->addItem(item);
    }
}

void add_link_button::on_films_list_itemClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    int id = 0;
    Film f = var.value<Film>();
    id = filmGetId(&f);
    this->film_id = id;
    this->ui->add_button->setEnabled(true);
}

void add_link_button::recieve_user_id(int id){
    this->user_id = id;
}
void add_link_button::recieve_actor_id(int id){
    this->actor_id = id;
    qDebug() << actor_id << "actor_id - recieve_id";
}


