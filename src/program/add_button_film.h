#ifndef ADD_BUTTON_FILM_H
#define ADD_BUTTON_FILM_H

#include <QDialog>
#include "film.h"

namespace Ui {
class add_button_film;
}

class add_button_film : public QDialog
{
    Q_OBJECT

public:
    explicit add_button_film(QWidget *parent = 0);
    ~add_button_film();

private slots:
    void on_cancel_button_clicked();

    void on_ok_button_clicked();

signals:
    void send_film(Film f);

private:
    Ui::add_button_film *ui;
};

#endif // ADD_BUTTON_FILM_H
