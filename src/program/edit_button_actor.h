#ifndef EDIT_BUTTON_ACTOR_H
#define EDIT_BUTTON_ACTOR_H

#include <QDialog>
#include <QFileDialog>
#include "actor.h"
#include "storage.h"
#include "film.h"
#include <string>
#include "storage.h"
#include "sqlite_storage.h"
#include <string>
#include <QDebug>
#include <QDialog>
#include <QListWidget>
#include "add_link_button.h"


namespace Ui {
class edit_button_actor;
}

class edit_button_actor : public QDialog
{
    Q_OBJECT
    Actor actor_;
    int actor_id;
    int user_id;
    int film_id;
    Storage *storage_;
    string image_;
    void fill_linked_list();

public:
    explicit edit_button_actor(QWidget *parent = 0);
    ~edit_button_actor();

private slots:
    void on_ok_button_clicked();

    void on_cancel_button_clicked();

    void on_delete_buton_clicked();

    void on_add_button_clicked();

    void on_edit_image_button_clicked();

    void on_linked_list_itemClicked(QListWidgetItem *item);

public slots:
    void receive_actor(Actor actor);

    void recieve_user_id(int user_id);

signals:
    void send_edited_actor(Actor actor);

    void send_film(Film film);

    void send_user_id(int id);

    void send_actor_id(int id);
private:
    Ui::edit_button_actor *ui;
    string &filmGetName(Film * self);
    int &filmGetId(Film * self);
};

#endif // EDIT_BUTTON_ACTOR_H
