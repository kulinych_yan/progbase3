#include "registration.h"
#include "ui_registration.h"
#include <QDebug>

registration::registration(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::registration)
{

    QString file_name = "/home/yan/progbase3/src/program/data/sql";
    qDebug() << file_name;
//    const string file_name  = "/home/yan/progbase3/data";
    if(storage_ == NULL)
    {
        qDebug() << "storage = NULL";
        delete storage_;
    }
    if(file_name == NULL)
    {
        qDebug() << "file = NULL";
    }

    SqliteStorage * sql_storage = new SqliteStorage(file_name.toStdString());
    Storage * storage = sql_storage;
    this->storage_ = storage;
    if(!this->storage_->open())
    {
        qDebug() << "storage not opened";
    }
    ui->setupUi(this);
    ui->lineEdit_password->setEchoMode(QLineEdit::Password);
}

registration::~registration()
{
    delete ui;
}

void registration::on_ok_button_clicked()
{
    SqliteStorage * sql_storage = new SqliteStorage("/home/yan/progbase3/src/program/data/sql");
    Storage * storage = sql_storage;
    this->storage_ = storage;
    if(!this->storage_->open())
    {
        qDebug() << "storage not opened";
    }
    User user;
    user.login = this->ui->lineEdit_login->text().toStdString();
    string pass_hash = this->ui->lineEdit_password->text().toStdString();
    qDebug() << "login: "<<QString::fromStdString(user.login);
    qDebug() <<"password: "<<QString::fromStdString(pass_hash);
    user.password = md5(pass_hash);
    vector<User> users = this->storage_->getAllUsers();
    for(User & u : users)
    {
        if(u.login == user.login)
        {
            this->ui->label->setText("This username is busy");
            return;
        }
    }
    this->storage_->insertUser(user);
    this->close();
    qDebug() << "OK!";
}

void registration::on_cancel_button_clicked()
{
    this->close();
}
