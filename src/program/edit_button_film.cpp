#include "edit_button_film.h"
#include "ui_edit_button_film.h"

edit_button_film::edit_button_film(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::edit_button_film)
{
    ui->setupUi(this);
}

edit_button_film::~edit_button_film()
{
    delete ui;
}

void edit_button_film::on_ok_button_clicked()
{
    this->film_.name = this->ui->line_name->text().toStdString();
    this->film_.genre = this->ui->line_genre->text().toStdString();
    this->film_.rating = this->ui->spinBox_rating->value();
    this->film_.yearOfIssue = this->ui->spinBox_date->value();
    this->send_edited_film(this->film_);
    this->close();
}

void edit_button_film::on_cancel_button_clicked()
{
    this->close();
}

void edit_button_film::receive_film(Film film)
{
    this->film_ = film;
    this->ui->line_name->setText(QString::fromStdString(film.name));
    this->ui->line_genre->setText(QString::fromStdString(film.genre));
    this->ui->spinBox_date->setValue(film.yearOfIssue);
    this->ui->spinBox_rating->setValue(film.rating);
    this->film_id = film.id;
}
