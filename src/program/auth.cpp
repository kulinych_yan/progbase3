#include "auth.h"
#include "ui_auth.h"

auth::auth(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::auth)
{
    ui->setupUi(this);
    ui->lineEdit_password->setEchoMode(QLineEdit::Password);
}

auth::~auth()
{
    delete ui;
}


void auth::on_ok_button_clicked()
{
    User user;
    user.login = this->ui->lineEdit_login->text().toStdString();
    string pass_hash = this->ui->lineEdit_password->text().toStdString();
    user.password = md5(pass_hash);
    send_data(user);
}

void auth::on_cancel_button_clicked()
{
    this->close();
}

void auth::get_auth(optional<User> replay){
    if(replay)
    {
        this->close();
    }
    else
    {
        this->ui->error_lable->setText("Incorrect login or password");
    }
}

void auth::on_pushButton_clicked()
{
    registration reg_dialog;
    reg_dialog.setModal(true);
    reg_dialog.exec();
}
