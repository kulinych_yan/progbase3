#ifndef REMOVE_BUTTON_FILM_H
#define REMOVE_BUTTON_FILM_H

#include <QDialog>

namespace Ui {
class remove_button_film;
}

class remove_button_film : public QDialog
{
    Q_OBJECT

public:
    explicit remove_button_film(QWidget *parent = 0);
    ~remove_button_film();

signals:
    void confirm_remove();

private slots:
    void on_ok_button_clicked();

    void on_cancel_button_clicked();

private:
    Ui::remove_button_film *ui;
};

#endif // REMOVE_BUTTON_FILM_H
