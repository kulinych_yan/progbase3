#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "add_button_actor.h"
#include "add_button_film.h"
#include "edit_button_actor.h"
#include "edit_button_film.h"
#include "remove_button_actor.h"
#include "remove_button_film.h"
#include "ui_mainwindow.h"
#include "auth.h"
#include <QMessageBox>
#include <QDebug>
#include <QFileDialog>
#include <QShortcut>
#include "storage.h"
#include "xml_storage.h"
#include "actor.h"
#include <film.h>
#include <fstream>
#include <QListWidget>
#include <QListWidgetItem>
#include <QFile>/*
#include <QtXml>
#include <QDomElement>*/
#include <vector>
#include "sqlite_storage.h"
#include "optional.h"
#include <QPixmap>
#include "xml_storage.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    Storage *storage_;
    string selected_entity;
    //bool m_isAuth;
    int user_id;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_actor_button_clicked();

    void on_film_button_clicked();

    void on_add_button_clicked();

    void on_edit_button_clicked();

    void on_remove_button_clicked();

    void on_info_list_itemClicked(QListWidgetItem *item);

    void on_actionClose_triggered();

    void on_actionNew_storage_triggered();

    void on_actionOpen_storage_triggered();

    void on_lineEdit_textEdited(const QString &arg1);

    void on_actionImport_actors_triggered();

    void on_actionImport_films_triggered();

    void on_actionExport_actors_triggered();

    void on_actionExport_films_triggered();

signals:
    void send_actor(Actor actor);

    void send_film(Film film);

    void send_reply(optional<User> replay);

    void send_user_id(int user_id);

public slots:
    void recieve_new_actor(Actor a);

    void recieve_new_film(Film f);

    void recieve_edited_actor(Actor a);

    void recieve_edited_film(Film f);

    void recieve_user_input(User info);

    void remove_actor();

    void remove_film();

private:
    Ui::MainWindow *ui;
    void fill_list_info_actors();
    void fill_list_info_films();
    void fill_info_list();
    void update_info_list();
    //int id_entity;
    QShortcut       *keyCtrlO;
    QShortcut       *keyCtrlN;
    string & actorGetName(Actor * self);
    string & filmGetName(Film * self);
    int & actorGetId(Actor * self);
    int & filmGetId(Film * self);
    bool xml_load_actors(string file_name);
    bool xml_load_films(string file_name);
};

#endif // MAINWINDOW_H
