#include "edit_button_actor.h"
#include "ui_edit_button_actor.h"
#include <QFileDialog>

string & edit_button_actor::filmGetName(Film * self)
{
    return self->name;
}

int & edit_button_actor::filmGetId(Film * self)
{
    return self->id;
}

edit_button_actor::edit_button_actor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::edit_button_actor)
{
//    QString file_name = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"/home/yan/progbase3/data/",QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks);
    QString file_name = "/home/yan/progbase3/src/program/data/sql";
    qDebug() << file_name;
    if(storage_ == NULL)
    {
        qDebug() << "storage = NULL";
        //delete storage_;
    }
    if(file_name == NULL)
    {
        qDebug() << "file = NULL";
    }

    SqliteStorage * sql_storage = new SqliteStorage(file_name.toStdString());
    Storage * storage = sql_storage;
    this->storage_ = storage;
    if(!this->storage_->open())
    {
        qDebug() << "storage not opened";
    }
    ui->setupUi(this);
    ui->delete_buton->setEnabled(false);
}

edit_button_actor::~edit_button_actor()
{
    delete ui;
}

void edit_button_actor::receive_actor(Actor actor)
{
    this->ui->line_name->setText(QString::fromStdString(actor.name));
    this->ui->line_country->setText(QString::fromStdString(actor.country));
    this->ui->spinBox_age->setValue(actor.age);
    this->ui->spinBox_film_amount->setValue(actor.filmAmount);
    int w = ui->label->width();
    int h = ui->label->height();
    string image_name = actor.image;
    QPixmap pix(image_name.c_str());
    this->ui->label->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
//    cout << image_name;
    this->actor_id = actor.id;
    qDebug() << this->actor_id << "actor id";
    this->image_ = actor.image;
    this->fill_linked_list();
}
int user_id1 = 3;

void edit_button_actor::on_ok_button_clicked()
{
    Actor actor;
    actor.name = this->ui->line_name->text().toStdString();
    actor.country = this->ui->line_country->text().toStdString();
    actor.age = this->ui->spinBox_age->value();
    actor.filmAmount = this->ui->spinBox_film_amount->value();
    actor.id = this->actor_id;
    //qDebug() << this->actor_id << "edited id";
    actor.image = this->image_;
    //
    this->send_edited_actor(actor);
    this->close();
}

void edit_button_actor::on_cancel_button_clicked()
{
    this->close();
}

void edit_button_actor::on_delete_buton_clicked()
{
    this->storage_->removeActorFilm(this->actor_id, this->film_id);
    ui->delete_buton->setEnabled(false);
    this->fill_linked_list();
}

void edit_button_actor::on_add_button_clicked()
{
    add_link_button link_dialog(NULL , user_id);
    //connect(&link_dialog, SIGNAL(send_film(Film)), this, SLOT(recieve_new_film(Film)));
    connect(this, SIGNAL(send_user_id(int)), &link_dialog, SLOT(recieve_user_id(int)));
    connect(this, SIGNAL(send_actor_id(int)), &link_dialog, SLOT(recieve_actor_id(int)));
    this->send_actor_id(actor_id);
    link_dialog.setModal(true);
    link_dialog.exec();
    this->fill_linked_list();
}

void edit_button_actor::fill_linked_list()
{
    ui->linked_list->clear();
    qDebug() << this->actor_id<<   "fill linked list";
    vector <Film> vec = storage_->getAllActorFilms(this->actor_id);
    qDebug() << vec.size();
    for (size_t i = 0; i < vec.size(); i++)
    {
        QVariant var;
        Film cur = vec.at(i);
        var.setValue(cur);
        QListWidgetItem *item = new QListWidgetItem();
        item->setText(QString::fromStdString(filmGetName(&cur)));
        item->setData(Qt::UserRole, var);
        ui->linked_list->addItem(item);
    }
}

void edit_button_actor::on_edit_image_button_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(
                   this,              // parent
                   "Dialog Caption",  // caption
                   "/home/yan/progbase3/src/program/data/img",                // directory to start with
                   "All Files (*)");  // file name filter
    if(fileName == NULL)
    {
        qDebug() << fileName << " file not opened!";
        return;
    }
    image_ = fileName.QString::toStdString();
    int w = ui->label->width();
    int h = ui->label->height();
    string image_name = this->image_;
    QPixmap pix(image_name.c_str());
    this->ui->label->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
}

void edit_button_actor::on_linked_list_itemClicked(QListWidgetItem *item)
{
    ui->delete_buton->setEnabled(true);
    QVariant var = item->data(Qt::UserRole);
    int id = 0;
    Film f = var.value<Film>();
    id = filmGetId(&f);
    this->film_id = id;
}

void edit_button_actor::recieve_user_id(int user_id)
{
    this->user_id = user_id;
}
