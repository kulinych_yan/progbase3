#pragma once

#include <string>
#include <QMetaType>

using std::string;

typedef struct Film_
{
    int id;
    string name;
    int rating;
    string genre;
    int yearOfIssue;
    int user_id;
}Film;

Q_DECLARE_METATYPE(Film)
