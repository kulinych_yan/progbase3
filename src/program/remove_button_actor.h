#ifndef REMOVE_BUTTON_ACTOR_H
#define REMOVE_BUTTON_ACTOR_H

#include <QDialog>

namespace Ui {
class remove_button_actor;
}

class remove_button_actor : public QDialog
{
    Q_OBJECT

public:
    explicit remove_button_actor(QWidget *parent = 0);
    ~remove_button_actor();

signals:
    void confirm_remove();

private slots:
    void on_ok_button_clicked();

    void on_cancel_button_clicked();

private:
    Ui::remove_button_actor *ui;
};

#endif // REMOVE_BUTTON_ACTOR_H
