#ifndef AUTH_H
#define AUTH_H

#include <QDialog>
#include <mainwindow.h>
#include "user.h"
#include "registration.h"
#include "optional.h"

namespace Ui {
class auth;
}

class auth : public QDialog
{
    Q_OBJECT

public:
    explicit auth(QWidget *parent = 0);
    ~auth();

public slots:
 void get_auth(optional<User> replay);


private slots:

    void on_ok_button_clicked();

    void on_cancel_button_clicked();

    void on_pushButton_clicked();

signals:
    void send_data(User user);

private:
    Ui::auth *ui;
};

#endif // AUTH_H
