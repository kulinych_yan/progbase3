#ifndef ADD_LINK_BUTTON_H
#define ADD_LINK_BUTTON_H

#include <QDialog>
#include "storage.h"
#include "sqlite_storage.h"
#include <QListWidget>


namespace Ui {
class add_link_button;
}

class add_link_button : public QDialog
{
    Q_OBJECT
    Storage * storage_;
    int film_id;
    int actor_id;
    void fill_films_list();

public:
    explicit add_link_button(QWidget *parent = 0, int user = 0);
    ~add_link_button();

private slots:
    void on_add_button_clicked();

    void on_close_button_clicked();

    void on_films_list_itemClicked(QListWidgetItem *item);

public slots:
    void recieve_user_id(int id);

    void recieve_actor_id(int actor_id);

private:
    Ui::add_link_button *ui;
    int user_id;
    int & filmGetId(Film * self);
    string & filmGetName(Film * self);
};
#endif // ADD_LINK_BUTTON_H
