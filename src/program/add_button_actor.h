#ifndef ADD_BUTTON_ACTOR_H
#define ADD_BUTTON_ACTOR_H

#include <QDialog>
#include "actor.h"

namespace Ui {
class add_button_actor;
}

class add_button_actor : public QDialog
{
    Q_OBJECT

public:
    explicit add_button_actor(QWidget *parent = 0);
    ~add_button_actor();

private slots:
    void on_ok_button_clicked();

    void on_cancel_button_clicked();

signals:
    void send_actor(Actor a);

private:
    Ui::add_button_actor *ui;
};

#endif // ADD_BUTTON_ACTOR_H
