#include "remove_button_actor.h"
#include "ui_remove_button_actor.h"

remove_button_actor::remove_button_actor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::remove_button_actor)
{
    ui->setupUi(this);
}

remove_button_actor::~remove_button_actor()
{
    delete ui;
}

void remove_button_actor::on_ok_button_clicked()
{
    this->confirm_remove();
    this->close();
}

void remove_button_actor::on_cancel_button_clicked()
{
    this->close();
}
