#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QDialog>
#include "user.h"
#include "storage.h"
#include "sqlite_storage.h"

namespace Ui {
class registration;
}

class registration : public QDialog
{
    Q_OBJECT
    Storage *storage_;

public:
    explicit registration(QWidget *parent = 0);
    ~registration();

private slots:
    void on_ok_button_clicked();

    void on_cancel_button_clicked();

private:
    Ui::registration *ui;
};

#endif // REGISTRATION_H
