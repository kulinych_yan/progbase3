#include "add_button_film.h"
#include "ui_add_button_film.h"

add_button_film::add_button_film(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add_button_film)
{
    ui->setupUi(this);
    this->ui->spinBox_date->setRange(0, 2019);
    this->ui->spinBox_rating->setRange(0, 10);
}

add_button_film::~add_button_film()
{
    delete ui;
}

void add_button_film::on_cancel_button_clicked()
{
    this->close();
}

void add_button_film::on_ok_button_clicked()
{
    Film f;
    f.genre = ui->line_genre->text().toStdString();
    f.name = ui->line_name->text().toStdString();
    f.rating = ui->spinBox_rating->value();
    f.yearOfIssue = ui->spinBox_date->value();
    this->send_film(f);
    this->close();
}
