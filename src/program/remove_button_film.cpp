#include "remove_button_film.h"
#include "ui_remove_button_film.h"

remove_button_film::remove_button_film(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::remove_button_film)
{
    ui->setupUi(this);
}

remove_button_film::~remove_button_film()
{
    delete ui;
}

void remove_button_film::on_ok_button_clicked()
{
    this->confirm_remove();
    this->close();
}

void remove_button_film::on_cancel_button_clicked()
{
    this->close();
}
