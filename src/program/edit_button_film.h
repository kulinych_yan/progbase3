#ifndef EDIT_BUTTON_FILM_H
#define EDIT_BUTTON_FILM_H

#include <QDialog>
#include "film.h"

namespace Ui {
class edit_button_film;
}

class edit_button_film : public QDialog
{
    Q_OBJECT
    Film film_;
    int film_id;

public:
    explicit edit_button_film(QWidget *parent = 0);
    ~edit_button_film();

private slots:
    void on_ok_button_clicked();

    void on_cancel_button_clicked();

public slots:
    void receive_film(Film f);

signals:
    void send_edited_film(Film f);

private:
    Ui::edit_button_film *ui;
};

#endif // EDIT_BUTTON_FILM_H
